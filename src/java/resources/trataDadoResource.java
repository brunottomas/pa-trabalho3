package resources;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.sql.*;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import resources.Convertor.*;
import org.json.*;

@Path("/dado")
public class trataDadoResource {
    //Inclui uma nova entrada na agenda
    @POST
    @Path("/{nome}/{sobrenome}/{email}/{telefone}")
    public String incluirDado(
            @PathParam("nome") String nome,
            @PathParam("sobrenome") String sobrenome,
            @PathParam("telefone") String telefone,
            @PathParam("email") String email
    ) throws ClassNotFoundException {
        //statement
        try {
            Connection conn = null;
            String sql = null;
            Statement stmt = null;
            conn = conectaBd.conectabd();
            stmt = conn.createStatement();
            sql = "INSERT INTO dados (nome, sobrenome, email, telefone) "
                    + "VALUES ('" + nome + "','" + sobrenome + "','" + email + "','" + telefone + "')";
            stmt.executeUpdate(sql);
            
        } catch (SQLException se) {
            se.printStackTrace();
        }

        return "{\"nome\":\"" + nome + "\","
                + "\"sobrenome\":\"" + sobrenome + "\","
                + "\"email\":\"" + email + "\","
                + "\"telefone\":\"" + telefone + "\"}";
    }
    //Lista todas as entradas da agenda
    @GET
    @Path("/lista")
    public String verAgenda() throws ClassNotFoundException, Exception {
        //Lista toda a agenda
        try {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rs = null;
            conn = conectaBd.conectabd();
            stmt = conn.createStatement();
            String query = "SELECT nome, sobrenome, email, telefone, id FROM DADOS "
                    + "ORDER BY nome";
            rs = stmt.executeQuery(query);
            JSONArray jsonArray = Convertor.convertToJSON(rs);
            return jsonArray.toString();

        } catch (SQLException se) {
            se.printStackTrace();
        }
        return "";
    }
    //Remove uma entrada
    @DELETE
    @Path("/remover/{id}")
    public String remover(@PathParam("id") String id) throws ClassNotFoundException {
        try {
            Connection conn = null;
            String sql = null;
            Statement stmt = null;
            conn = conectaBd.conectabd();
            stmt = conn.createStatement();
            sql = "DELETE FROM dados "
                    + "WHERE id = '" + id + "';";
            stmt.executeUpdate(sql);
        } catch (SQLException se) {
            se.printStackTrace();
        }

        return "{\"id\":\"" + id + "\"}";

    }
    //Abre os detalhes de uma entrada
    @GET
    @Path("/ver/{id}")
    public String ver(@PathParam("id") String id) throws ClassNotFoundException, Exception {
        try {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rs = null;
            conn = conectaBd.conectabd();
            stmt = conn.createStatement();
            String query = "SELECT nome, sobrenome, email, telefone, id FROM dados "
                    + "WHERE id = " + id + ";";
            rs = stmt.executeQuery(query);
            JSONArray jsonArray = Convertor.convertToJSON(rs);
            return jsonArray.toString();

        } catch (SQLException se) {
            se.printStackTrace();
        }
        return "";
    }
    //Altera uma entrada
    @PUT
    @Path("/alterar/{id}/{nome}/{sobrenome}/{email}/{telefone}")
    public String editar(@PathParam("id") String id,
            @PathParam("nome") String nome,
            @PathParam("sobrenome") String sobrenome,
            @PathParam("email") String email,
            @PathParam("telefone") String telefone) throws ClassNotFoundException {
        try {
            Connection conn = null;
            String sql = null;
            Statement stmt = null;
            conn = conectaBd.conectabd();
            stmt = conn.createStatement();
            sql = "UPDATE dados "
                    + "SET nome = '"+nome+"', sobrenome = '"+sobrenome+"', email ='"+email+"', telefone='"+telefone+"' "
                    + "WHERE id = '" + id + "';";
            stmt.executeUpdate(sql);
        } catch (SQLException se) {
            se.printStackTrace();
        }

        return "{\"id\":\"" + id + "\"}";
    }
    //Busca
    @GET
    @Path("/busca/{termo}")
    public String buscar(@PathParam("termo") String termo) throws ClassNotFoundException, Exception{
        try{
            Connection conn = null;
            Statement stmt = null;
            ResultSet rs = null;
            conn = conectaBd.conectabd();
            stmt = conn.createStatement();
            String query = "SELECT nome, sobrenome, email, telefone FROM dados "
                    + "WHERE (nome LIKE '%"+termo+"%' OR sobrenome LIKE '%"+termo+"%') "
                    + "ORDER BY nome;";
            System.out.println(query);
            rs = stmt.executeQuery(query);
            JSONArray jsonArray = Convertor.convertToJSON(rs);
            return jsonArray.toString();
            
            
        }catch (SQLException se) {
            se.printStackTrace();
        }
        return "";
    }
}
