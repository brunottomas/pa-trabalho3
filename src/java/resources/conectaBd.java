
package resources;
import java.sql.*;

public class conectaBd {

    public static Connection conectabd() throws ClassNotFoundException {
        Connection con = null;
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/agenda", "postgres", "admin");
            System.out.println("Conectado ao banco de dados!");
        }
        catch(SQLException error){
            System.out.println("ERRO:"+error.toString());    
        }
        return con;
    }    
    
}
