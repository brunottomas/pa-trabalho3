
package resources;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

//Recurso de teste para verificar se o REST funciona chamando /resources/hello

@Path("/hello")
public class helloResource {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String sayHello(){
        return "Hello world!";
    }
}
