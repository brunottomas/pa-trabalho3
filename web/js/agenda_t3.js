/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

document.addEventListener("DOMContentLoaded", abreAgenda());
function limpaCampos() {
    document.getElementById("nome").value = "";
    document.getElementById("sobrenome").value = "";
    document.getElementById("telefone").value = "";
    document.getElementById("email").value = "";
}

function abrePagAdicionar() {
    document.getElementById("pag_adicionar").style.display = "block";
    document.getElementById("pag_visualizar").style.display = "none";
    document.getElementById("pag_editar").style.display = "none";
    document.getElementById("pag_busca").style.display = "none";
    document.getElementById("adiciona").style.display = "none";
    document.getElementById("ver").style.display = "block";
    document.getElementById("buscar").style.display = "block";

}

function abrePagAgenda() {
    document.getElementById("pag_adicionar").style.display = "none";
    document.getElementById("pag_visualizar").style.display = "block";
    document.getElementById("pag_editar").style.display = "none";
    document.getElementById("pag_busca").style.display = "none";
    document.getElementById("adiciona").style.display = "block";
    document.getElementById("ver").style.display = "none";
    document.getElementById("buscar").style.display = "block";
}

function abrePagBusca() {
    document.getElementById("pag_adicionar").style.display = "none";
    document.getElementById("pag_visualizar").style.display = "none";
    document.getElementById("pag_editar").style.display = "none";
    document.getElementById("pag_busca").style.display = "block";
    document.getElementById("adiciona").style.display = "block";
    document.getElementById("ver").style.display = "block";
    document.getElementById("buscar").style.display = "none";

}

function adicionaNovaEntrada() {
    var nome = document.getElementById("nome").value;
    var sobrenome = document.getElementById("sobrenome").value;
    var telefone = document.getElementById("telefone").value;
    var email = document.getElementById("email").value;
    var path = "resources/dado/" + nome + "/" + sobrenome + "/" + email + "/" + telefone;
    if (nome === "" || sobrenome === "" || telefone === "" || email === "") {
        document.getElementById("mensagem").innerHTML = "Favor preencher todos os campos";
        console.log("[adicionar()] ERRO: campos deixados em branco");
    }

    var ajaxRequest = new XMLHttpRequest();
    ajaxRequest.open("POST", path);
    ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    ajaxRequest.onreadystatechange =
            function () {
                if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
                    console.log('=== Recebeu resposta: ' + ajaxRequest.responseText);
                    document.getElementById("mensagem").innerHTML = "Nome adicionado com êxito";
                }
                else {
                    console.log("[adicionar()] ERRO: ");
                }
                ;
            };
    ajaxRequest.send(null);
    console.log("OK");
    setTimeout("location.reload(true)", 2000);
}


function abreAgenda() {
    var path = "resources/dado/lista";
    ajaxRequest = new XMLHttpRequest();
    ajaxRequest.open("GET", path);
    ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    ajaxRequest.onreadystatechange =
            function () {
                if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
                    var json = ajaxRequest.responseText;
                    console.log('=== Recebeu resposta: ' + ajaxRequest.responseText);
                    //document.getElementById("tabela").innerHTML=ajaxRequest.responseText;
                    var parsedJson = JSON.parse(json);
                    //console.log(parsedJson);
                    var table =
                            "<table border='1' >\n\
                            <tr>\n\
                            <td>Nome</td>\n\
                            <td>Sobrenome</td>\n\
                            <td>e-mail</td>\n\
                            <td>Telefone</td>\n\
                            <td>Editar</td>\n\
                            <td>Remover</td>\n\
                            </tr>";
                    parsedJson.forEach(function (object) {
                        var line = "<tr>\n\
                            <td>" + object.nome + "</td>\n\
                            <td>" + object.sobrenome + "</td>\n\
                            <td>" + object.email + "</td>\n\
                            <td>" + object.telefone + "</td>\n\
                            <td> <a href=# onclick=\"abreEdicao(" + object.id + ")\">Editar</a></td>\n\
                            <td> <a href=# onclick=\"remover(" + object.id + ")\">Remover</a></td>\n\
                            </tr>";
                        table += line;
                    }
                    );
                    table += "</table>";
                    document.getElementById("tabela").innerHTML = table;
                }

                else {
                    console.log("[adicionar()] ERRO: ");
                }
                ;
            };
    ajaxRequest.send(null);
    console.log("OK");
}

function remover(id) {
    var r = confirm("Deseja realmente remover?");
    if (r === true) {
        var path = "resources/dado/remover/" + id;
        ajaxRequest = new XMLHttpRequest();
        ajaxRequest.open("DELETE", path);
        ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        ajaxRequest.onreadystatechange =
                function () {
                    if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
                        console.log('=== Recebeu resposta: ' + ajaxRequest.responseText);
                        document.getElementById("prompt").innerHTML = "Nome deletado com êxito";
                    }
                    else {
                        console.log("[adicionar()] ERRO: ");
                    }
                    ;
                };
        ajaxRequest.send(null);
        console.log("OK");
        setTimeout("location.reload(true)", 2000);
    }
}

function abreEdicao(id) {
    document.getElementById("pag_adicionar").style.display = "none";
    document.getElementById("pag_visualizar").style.display = "none";
    document.getElementById("pag_editar").style.display = "block";
    document.getElementById("pag_busca").style.display = "none";
    document.getElementById("ver").style.display = "block";
    document.getElementById("adiciona").style.display = "block";
    document.getElementById("buscar").style.display = "block";

    var path = "resources/dado/ver/" + id;
    ajaxRequest = new XMLHttpRequest();
    ajaxRequest.open("GET", path);
    ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    ajaxRequest.onreadystatechange =
            function () {
                if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
                    var json = ajaxRequest.responseText;
                    console.log('=== Recebeu resposta: ' + ajaxRequest.responseText);
                    //document.getElementById("tabela").innerHTML=ajaxRequest.responseText;
                    var parsedJson = JSON.parse(json)[0];
                    document.getElementById("ed_nome").value = parsedJson.nome;
                    document.getElementById("ed_sobrenome").value = parsedJson.sobrenome;
                    document.getElementById("ed_email").value = parsedJson.email;
                    document.getElementById("ed_telefone").value = parsedJson.telefone;
                    document.getElementById("ed_id").value = parsedJson.id;
                }
                else {
                    console.log("[adicionar()] ERRO: ");
                }
                ;
            };
    ajaxRequest.send(null);
    console.log("OK");

}

function editar(id) {
    var r = confirm("Deseja realmente alterar?");
    if (r === true) {
        var nome = document.getElementById("ed_nome").value;
        var sobrenome = document.getElementById("ed_sobrenome").value;
        var telefone = document.getElementById("ed_telefone").value;
        var email = document.getElementById("ed_email").value;
        var path = "resources/dado/alterar/" + id + "/" + nome + "/" + sobrenome + "/" + email + "/" + telefone;
        if (nome === "" || sobrenome === "" || telefone === "" || email === "") {
            document.getElementById("mensagem").innerHTML = "Favor preencher todos os campos";
            console.log("[adicionar()] ERRO: campos deixados em branco");
        }
        ajaxRequest = new XMLHttpRequest();
        ajaxRequest.open("PUT", path);
        ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        ajaxRequest.onreadystatechange =
                function () {
                    if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
                        console.log('=== Recebeu resposta: ' + ajaxRequest.responseText);
                        document.getElementById("ed_mensagem").innerHTML = "Alterado com êxito";
                    }
                    else {
                        console.log("[adicionar()] ERRO: ");
                    }
                    ;
                };
        ajaxRequest.send(null);
        console.log("OK");
        setTimeout("location.reload(true)", 2000);
    }
}

function busca() {
    var termo = document.getElementById("busca").value;
    var path = "resources/dado/busca/" + termo;
    ajaxRequest = new XMLHttpRequest();
    ajaxRequest.open("GET", path);
    ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    ajaxRequest.onreadystatechange =
            function () {
                if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
                    var json = ajaxRequest.responseText;
                    console.log('=== Recebeu resposta: ' + ajaxRequest.responseText);
                    var parsedJson = JSON.parse(json);
                    var table =
                            "<table border='1'>\n\
                            <tr>\n\
                            <td>Nome</td>\n\
                            <td>Sobrenome</td>\n\
                            <td>e-mail</td>\n\
                            <td>Telefone</td>\n\
                            <td>Editar</td>\n\
                            <td>Remover</td>\n\
                            </tr>";
                    parsedJson.forEach(function (object) {
                        var line = "<tr>\n\
                            <td>" + object.nome + "</td>\n\
                            <td>" + object.sobrenome + "</td>\n\
                            <td>" + object.email + "</td>\n\
                            <td>" + object.telefone + "</td>\n\
                            <td> <a href=# onclick=\"abreEdicao(" + object.id + ")\">Editar</a></td>\n\
                            <td> <a href=# onclick=\"remover(" + object.id + ")\">Remover</a></td>\n\
                            </tr>";
                        table += line;
                    }

                    );
                    table += "</table>";
                    document.getElementById("res_busca").innerHTML = table;
                }
                else {
                    console.log("[adicionar()] ERRO: ");
                }


            };
    ajaxRequest.send(null);
    console.log("OK");
}