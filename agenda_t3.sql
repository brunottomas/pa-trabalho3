--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dados; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dados (
    nome character varying(20) NOT NULL,
    sobrenome character varying(40) NOT NULL,
    email character varying(50) NOT NULL,
    telefone character varying(20) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE dados OWNER TO postgres;

--
-- Name: dados_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dados_id_seq OWNER TO postgres;

--
-- Name: dados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dados_id_seq OWNED BY dados.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dados ALTER COLUMN id SET DEFAULT nextval('dados_id_seq'::regclass);


--
-- Data for Name: dados; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dados (nome, sobrenome, email, telefone, id) FROM stdin;
Skyler	White	skyler.white@beneke.com	2222-2222	16
Hank	Schrader	schrader@dea.gov	4444-4444	18
Walter	White	saymyname@heisenberg.com	1111-1111	15
Jesse	Pinkman	yobitch@yahoo.com	5555-5555	19
Gustavo	Fring	gus@pollos.com	6666-6666	20
Walter	White Jr.	theflynn@gmail.com	3333-3333	17
Saul	Goodman	bettercall@saul.com	7777-7777	21
\.


--
-- Name: dados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dados_id_seq', 21, true);


--
-- Name: dados_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dados
    ADD CONSTRAINT dados_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

